<?php
include "koneksi.php";
if(isset($_GET['no_prakerja'])) {
    $no_prakerja = $_GET['no_prakerja'];
   
    // echo $no_prakerja;
    // exit();
    //pquery untuk menghapus data ke database mysql
    $q = mysqli_query($koneksi,"delete from t_prakerja where no_prakerja='$no_prakerja'");

    if($q) {
        echo "<script>alert('Hapus Data Berhasil');window.location.href='index.php';</script>";
    } else {
        die("Hapus data gagal :".mysqli_connect_error());
    }
}

?>
<html>
    <head>
        <title>Manajemen Data Prakerja</title>
    </head>
    <body>
        <h2>Manajemen CRUD Data Prakerja</h2>
        <a href="tambah.php">+ Tambah Data</a>
        <br>
        <br>
        <form action="" method="post">
            <table class="table">
                <thead>
                    <tr>
                        <th>Silakan isi data yang anda cari</th>
                        <th>:</th>
                        <th><input type="text" name="cari">
                      <button type="submit">Cari</button></th>
                    </tr>
                </thead>
            </table>
        </form>
        <table class="table" border="1">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>No. Kartu Prakerja</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>Opsi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(isset($_POST['cari'])) {
                    $cari = $_POST['cari'];
                    if($cari=='Laki-laki' || $cari=='Perempuan') {
                        if($cari=='Laki-laki') $cari='L'; else $cari='P';
                        $data = mysqli_query($koneksi,"select * from t_prakerja where 
                        jenis_kelamin='$cari'");
                    } else {
                        $data = mysqli_query($koneksi,"select * from t_prakerja where 
                        no_prakerja like '%$cari%' or nama like '%$cari%' or alamat like '%$cari%'");
                    }
                } else {
                    $data = mysqli_query($koneksi,"select * from t_prakerja");
                }
                $no=1;
                while ($r = mysqli_fetch_array($data)):
                ?>
                <tr>
                    <td><?php echo $no;?></td>
                    <td><?php echo $r['no_prakerja'];?></td>
                    <td><?php echo $r['nama'];?></td>
                    <td><?php echo ($r['jenis_kelamin']=='L')?"Laki-laki":"Perempuan";?></td>
                    <td><?php echo $r['alamat'];?></td>
                    <td><a href="edit.php?no_prakerja=<?php echo $r['no_prakerja'];?>">EDIT</a> | 
                    <a href="javascript:hapusData('index.php?no_prakerja=<?php echo $r['no_prakerja'];?>','<?php echo $r['nama'];?>')">HAPUS</a></td>
                </tr>
                <?php $no++; endwhile; ?>
            </tbody>
        </table>
    </body>
</html>
<script>
    function hapusData(urlHapus,data) {
        if(confirm("Apakah anda yakin untuk menghapus atas nama "+data+"?")){
            window.location= urlHapus;
        }
    }
</script>