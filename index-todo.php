<?php
$todos=[];

if(file_exists("todo.txt")) {
    $file = file_get_contents("todo.txt");
    $todos = unserialize($file);
}

if(isset($_POST['todo'])) {
    $data = $_POST['todo'];
    $todos[] = [
        "todo" => $data,
        "status" =>0
    ];
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:index.php");
}
if(isset($_GET['status'])) {
    $todos[$_GET['key']]['status'] = $_GET['status'];
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:index.php");
}
if(isset($_GET['id'])){
    unset($todos[$_GET['id']]);
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:index.php");
}
// echo "<pre>";
// print_r($todos);
// echo "</pre>";

?>
<h2>Todo Apps</h2>
<form action="" method="post">
    <label>Daftar Belanja Hari ini</label><br>
    <input type="text" name="todo">
    <button type="submit">Simpan</button>
</form>
<ul>
    <?php foreach($todos as $key => $value): ?>
    <li>
        <input type="checkbox" name="todo" onclick="window.location.href='index.php?status=<?php echo ($value['status']=='1')?'0':'1';?>&key=<?php echo $key;?>'" 
        <?php echo ($value['status']=='1') ? 'checked':''; ?>>
        <label><?php echo ($value['status']=='1') ? "<del>".$value['todo']."</del>" : $value['todo'];?></label>
        <a href="javascript:hapusData('index.php?id=<?php echo $key;?>','<?php echo $value['todo'];?>')">Hapus</a>
    </li>
    <?php endforeach; ?>
</ul>
<script>
    function hapusData(urlHapus,data){
        if(confirm("Apakah anda yakin untuk menghapus daftar belanja "+data+"?")){
            window.location = urlHapus;
        }
    }
</script>