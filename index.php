<?php
$todos=[];

if(file_exists("todo.txt")) {
    $file = file_get_contents("todo.txt");
    $todos = unserialize($file);
}

if(isset($_POST['todo'])) {
    $data = $_POST['todo'];
    $todos[] = [
        "todo" => $data,
        "status" =>0
    ];
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:index.php");
}
if(isset($_GET['status'])) {
    $todos[$_GET['key']]['status'] = $_GET['status'];
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:index.php");
}
if(isset($_GET['id'])){
    unset($todos[$_GET['id']]);
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:index.php");
}
// echo "<pre>";
// print_r($todos);
// echo "</pre>";

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="todo.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <title>Aplikasi Todo</title>
  </head>
  <body>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="row container d-flex justify-content-center">
            <div class="col-md-12">
                <div class="card px-3">
                    <div class="card-body">
                        <h4 class="card-title">Todo Apps</h4>
                        <form action="" method="post">
                        <div class="add-items d-flex"> <input type="text" name="todo" class="form-control todo-list-input" placeholder="Daftar Belanja Hari ini"> <button type="submit" class="add btn btn-primary font-weight-bold todo-list-add-btn">Simpan</button> </div>
                        </form>
                        <div class="list-wrapper">
                            <ul class="d-flex flex-column-reverse todo-list">
                            <?php foreach($todos as $key => $value): ?>
    <li>
        <input type="checkbox" name="todo" onclick="window.location.href='index.php?status=<?php echo ($value['status']=='1')?'0':'1';?>&key=<?php echo $key;?>'" 
        <?php echo ($value['status']=='1') ? 'checked':''; ?>>
        <label><?php echo ($value['status']=='1') ? "<del>".$value['todo']."</del>" : $value['todo'];?></label>
       
        &nbsp;<a class="btn btn-danger" role="button" href="javascript:hapusData('index.php?id=<?php echo $key;?>','<?php echo $value['todo'];?>')">Hapus</a>
    </li>
    <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    function hapusData(urlHapus,data){
        if(confirm("Apakah anda yakin untuk menghapus daftar belanja "+data+"?")){
            window.location = urlHapus;
        }
    }
</script>