<?php

$books = [
    "Panduan belajar PHP untuk pemula",
    "Membangun aplikasi web dengan php",
    "Tutorial aplikasi Bot Chat dengan PHP",
    "Belajar PHP MySQL"
];

echo "<h5>Judul Buku PHP:</h5>";
echo "<ol type='a'>";
foreach($books as $buku) {
    echo "<li>$buku</li>";
}
echo "</ol>";