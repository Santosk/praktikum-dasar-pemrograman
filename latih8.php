<?php

function perkenalan($nama, $thn_lahir, $salam="Assalamu'alaikum") {
    echo "$salam,<br>";
    echo "Perkenalkan nama saya $nama<br>";
    echo "Umur saya adalah ".hitungUmur($thn_lahir, date('Y')). ' tahun<br>'; 
    echo "Senang berkenalan dengan anda<br>";
    echo "<hr>";
}
function hitungUmur($thn_lahir, $thn_skrng){
    $umur = $thn_skrng - $thn_lahir;
    return $umur;
}

perkenalan("Ardianta",2000);
perkenalan("Johan", 1991,"Selamat Siang");
perkenalan("Putu",1990,"Selamat Sore");


