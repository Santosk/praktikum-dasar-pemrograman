<?php

function faktorial($angka) {
    if($angka < 2){
        return 1;
    } else {
        return ($angka * faktorial($angka -1));
    }
}

$angka = 3;
echo "Faktorial $angka adalah : ".faktorial($angka);