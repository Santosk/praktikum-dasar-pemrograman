<?php
include "koneksi.php";
if(isset($_POST['no_prakerja'])) {
    $no_prakerja = $_POST['no_prakerja'];
    $nama = $_POST['nama'];
    $jenis_kelamin = $_POST['jenis_kelamin'];
    $alamat = $_POST['alamat'];
    // echo $no_prakerja;
    // exit();
    //pquery untuk menambah data ke database mysql
    $q = mysqli_query($koneksi,"insert into t_prakerja (no_prakerja,nama,alamat,jenis_kelamin) 
    values ('$no_prakerja','$nama','$alamat','$jenis_kelamin')");

    if($q) {
        echo "<script>alert('Input Data Berhasil');window.location.href='index.php';</script>";
    } else {
        die("Input data gagal :".mysqli_connect_error());
    }
}
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">

    <title>Penambahan Data Prakerja</title>
  </head>
  <body>
  

  <div class="content">
    
    <div class="container">
      <h2 class="mb-5">Tambah Data Prakerja</h2>
      <a class="btn btn-primary" role="button" href="index.php"><= Kembali</a>

      <div class="table-responsive custom-table-responsive">
      <form action="" method="post">
        <table class="table custom-table">
        <tr>
                
                <td colspan="3" >
                <div class="form-group">
                  <label for="">No. Kartu Prakerja</label>
                  <input type="text" class="form-control" name="no_prakerja" id="no_prakerja" aria-describedby="helpId" placeholder="No. Kartu Prakerja">
                  
                </div>  
                </td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td><input type="text" name="nama"></td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td><select name="jenis_kelamin">
                    <option value="">==PILIH==</option>
                    <option value="L">Laki-laki</option>
                    <option value="P">Perempuan</option>
                </select></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>
                  <textarea class="form-control" name="alamat" rows="3"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center"><button type="submit">Simpan</button></td>
                
            </tr>
        </table>
      </div>


    </div>

  </div>
    
    

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script>
    function hapusData(urlHapus,data) {
        if(confirm("Apakah anda yakin untuk menghapus atas nama "+data+"?")){
            window.location= urlHapus;
        }
    }
</script>
  </body>
</html>